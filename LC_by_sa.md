This [Label Commons Public Licence](https://git.fairkom.net/legal/lcpl) is intended to give the public the permission to use a label under certain conditions. Labels are used in the sense of a sign, term or mark applied to digital media or tangible material. The usage of the label without this license may be very restricted by copyright and/or trademark registration procedures now or in future.

A typical use case is that a label which is used by a wider community can now be provided by a so-called watchdog organisation, which even may register the label as a trademark and at the same time grants usage rights with only a few restrictions to the extend necessary to prevent misleading usage.

This license has been developed by the not-for-profit fairkom association, that helps to preserve common goods mainly by promoting the usage of free software. It was an affiliate for Creative Commons in Austria for many years and an active partner in CC's translation and interpretation processes. For the European Union Public License 1.2 fairkom had successfully proposed to extend its application also to works and not only to software. The CC-by-sa 4.0 and the EUPL 1.2 have been used as a template and were extended with moral rights and trademark rules.

Please submit any proposals for improvements as an [issue in the git repo](https://git.fairkom.net/legal/lcpl/issues).  We are also eager to know your use case.

# Label Commons 1.0 Public Licence

By exercising the Licensed Rights (defined below), You accept and agree to be bound by the terms and conditions of this Label Commons Public License ("Public License"). To the extent this Public License may be interpreted as a contract, You are granted the Licensed Rights in consideration of Your acceptance of these terms and conditions, and the Licensor grants You such rights in consideration of benefits the Licensor receives from making the Licensed Material available under these terms and conditions.

## 1. Definitions

In this License, the following terms have the following meaning:

* Adapted Material means material subject to Copyright and Similar Rights that is derived from or based upon the Licensed Material and in which the Licensed Material is translated, altered, arranged, transformed, or otherwise modified in a manner requiring permission under the Copyright and Similar Rights held by the Licensor.

* Class ist the International (Nice) Classification of Goods and Services.

* LCPL Notice is a short description to be applied to all Licensed Material.

* IPR Authority offers a brand or design registration service with national or international coverage (for example the EU Office for Harmonization in the Internal Market or a national IPR office).

* Label is the label as text

* Licensor means the individual(s) or entity(ies) granting rights under this Public License. Licensed Material means the elements forming the Label, and applying it hetherto, or other material to which the Licensor applied this Public License.

* Link to License Publishing Service is an URI either in ASCII or provided as QR Code. A\
  License Publishing Service is any publicly accessible online service that publishes the Label together with the license and its options longer than the license duration DATE.

* Region means a continental or political region as a geographical restriction of applicability.

* Timestamping Service is an online service qualified by a Timestamping Authority as defined in IETF RFC 3161 (for example RegisteredCommons.org).

* You means the individual or entity exercising the Licensed Rights under this Public License. Your has a corresponding meaning.

## 2. Scope

The Licensor hereby grants You a royalty-free, non-exclusive, sub-licensable, irrevocable license to:

A. reproduce and Share the Licensed Material, in whole or in part; and\
B. produce, reproduce, and Share Adapted Material.

The Licensor authorizes You to exercise the Licensed Rights in all media and formats whether now known or hereafter created, and to make technical modifications necessary to do so. The Licensor waives and/or agrees not to assert any right or authority to forbid You from making technical modifications necessary to exercise the Licensed Rights, including technical modifications necessary to circumvent Effective Technological Measures. For purposes of this Public License, simply making modifications authorized by this Section never produces Adapted Material.

Every recipient of the Licensed Material automatically receives an offer from the Licensor to exercise the Licensed Rights under the same terms and conditions of this Public License.

## 3. License conditions

The duration may be limited by a trademark or design registration period (DATE).

The reach may be limited to a trademark or design registration REGION(s). If none is defined, the license is valid world-wide.

The scope may be limited to one or more trademark or design registration CLASSes.

The Licensor may announce a MORAL restriction together with the LCPL notice, such as "No usage allowed in military context".

The Label is provided under the terms of this Licence when the Licensor has expressed his or her willingness to license under the LCPL, by publishing the LCPL Notice as follows:


> The label LABEL is offered as Label Commons (https://frkm.eu/LC_by_sa_v1) [until DATE] [in REGIONs] [for CLASSes] without Warranty [by LICENSOR] [registered at Name of IPR Authority] [& Link to License Publishing Service] [under the following additional conditions: MORAL].

Restrictions in \[brackets\] are issued by Licensor optionally, depending on which rights Licensor is able to share. Rights given to You should never be less then the rights Licensor holds, except moral rights restrictions, which can be defined by Licensor.

If the \[by LICENSOR\] option is chosen, You may need to give attribution with the Licensed Material, in any reasonable manner requested by the Licensor (including by pseudonym if designated) within the LCPL Notice as defined above, including a hyperlink to the extent reasonably practicable to the full LCPL Notice.

If the licensor is using a License Publishing Service then You may shorten the License Notice with a hyperlink (in ASCII or QR Code) to:

> Label Commons -- Link to License Publishing Service --


If You Share Adapted Material You produce, You have to apply the same or a higher version of the LCPL including the same conditions and restrictions as the original licensor had applied. You may not offer or impose any additional or different terms or conditions on, or apply any Effective Technological Measures to, Adapted Material that restrict exercise of the rights granted under the Adapter's License You apply.

## 4. Withdrawal of Rights

Applying a LCPL licence to a Label is irrevocable by the licensor.

If unforeseen limitations apply or others successfully claim rights to the Label, a revocation has to be announced by the licensor, preferably at the same Label Publishing Service the license was announced.

The licensor assumes no liability for the legal validity of the licensed Label. The licensor does not warrant that the licensed Labels are free of third-party rights now or in future.

The licensor does not know of any older third-party rights that could oppose the registration and use of the licensed Label. However, any warranty for the non-existence of such third-party rights is explicitly ruled out.

## 5. Disclaimer of Liability

fairkom is not a party to its public licenses unless it acts as a “Licensor.” fairkom is not a law firm and does not provide legal services or legal advice. Distribution of the Label Sharing Public License does not create a lawyer-client or other relationship. fairkom makes its licenses and related information available on an “as-is” basis. fairkom gives no warranties regarding its licenses, any material licensed under their terms and conditions, or any related information. fairkom disclaims all liability for damages resulting from their use to the fullest extent possible.

## 6. Termination

This Public License applies for the duration of the Copyright, Trade Mark and Similar Rights licensed here and as long as no other party than the Licensor has claimed ownership on such rights.

If You fail to comply with this Public License, then Your rights under this Public License terminate automatically.

Where Your right to use the Licensed Material has terminated under Section 6(a), it reinstates: automatically as of the date the violation is cured, provided it is cured within 30 days of Your discovery of the violation; or upon express reinstatement by the Licensor.

For the avoidance of doubt, this Section does not affect any right the Licensor may have to seek remedies for Your violations of this Public License.

Sections 1, 5, 6, 7 survive termination of this Public License.

## 7. Miscellaneous

If any provision of the Licence is invalid or unenforceable under applicable law, this will not affect the validity or enforceability of the Licence as a whole. Such provision will be construed or reformed so as necessary to make it valid and enforceable.

Without prejudice to specific agreement between parties,

— this Licence shall be governed by the law of the European Union Member State where the Licensor has her seat, resides or has her registered office,

— this licence shall be governed by Austrian law if the Licensor has no seat, residence or registered office inside a European Union Member State.

Any litigation resulting from the interpretation of this Public License, arising between fairkom association, as a Licensor, and any Licensee, will be subject to the jurisdiction of the Court of Justice of Austria.
This Label Commons Public Licence (LCPL) is intended to give the public the permission to use a label under certain conditions. Labels are used as a sign, term or mark and can be applied to digital media or tangible material. The usage of labels without applying a LCPL license may be restricted by other organisations or companies who claim copyright and/or register them as a trademark. 

A typical use case for LCPL is clarifying usage right of a label used by a wider community. It may be protected by a so-called watchdog organisation, which could be an NGO, a trusted partner organisation or a foundation. Its role would be to claim ownership and probably even register it as a trademark. At the same time the watchdog organisation grants usage rights with only a few restrictions to the extend necessary to prevent misleading or unintended usage, such as making a business with a label of a movement.

### License Conditions

The Label Commons Public Licence grants you the right to

*   reproduce and share the licensed material, in whole or in part; and       
*   produce, reproduce, and share adapted material

by respecting the following conditions

*   The coverage may be limited to a trademark or design registration REGION(s). If none is defined, the license is valid world-wide.
*   The scope may be limited to one or more trademark or design registration CLASSes.
*   The Licensor may announce a MORAL restriction together with the LCPL notice, such as "No application of label allowed to goods violating human dignity or to make for-profit business."

### License Application

> The label LABEL is offered as Label Commons (https://git.fairkom.net/legal/lcpl) [until DATE] [in REGIONs] [for CLASSes] without Warranty [by LICENSOR] [registered at Name of IPR Authority] [& Link to License Publishing Service] [under the following additional conditions: MORAL].

[brackets] are optional parameters

[Here](https://git.fairkom.net/legal/lcpl/blob/master/LC_by_sa.md) is the **Legal Code** of the license.  

### License Example 1

[**fairmove**](https://fairmove.org) is offered as a generic label which may be used by sustainability initiatives. The label aims to prevent commons-washing or ambiguity (like we have seen with the term "sustainability"). We apply a LCPL license as follows:

The label **fairmove** is offered as Label Commons (https://git.fairkom.net/legal/lcpl) without Warranty by fairkom association under the following additional conditions: The fairmove label may be used in spoken language and as a hashtag in social media to support and promote climate friendly and ethical behaviour and activities. Guiding values are the [Nine Ethify Values](https://ethify.org/en/content/values). Commercial use is restricted to small companies investing their surplus in sustainable products and services only or reach a minimum Economy of Common Goods score of 650; others may request a special license.

### License Example 2

**PodcastForFuture** is offered as a generic label which may be used by sustainability initiatives and journalists.

The label **PodcastForFuture** is offered as Label Commons (https://git.fairkom.net/legal/lcpl) without Warranty by Ökoligenta under the following additional conditions: The PodcastForFuture label may be used for radio shows that support and promote climate friendly and ethical behaviour and activities. Guiding values are the 17 UN SDGs, the Human Rights Declaration, the 9 Ethify Values or any of the guidelines listed in the MoU of wandelbuendnis.org. Commercial use is restricted to small companies investing their surplus in sustainable products and services only; others may request a special license.

(This is the [result of a Makers4Humanity working group](https://wechange.de/group/m4h-makers-for-humanity/document/wandel-podcast/) made on 23rd of August 2019 on providing a podcast channel, after several rounds of discussion on the brand and who may use it).

### Feedback

Research and previous considerations of this license have been archived here https://board.net/p/BSL

Please send your comments to info@fairkom.eu - we are eager to know more about your use case or submit an issue on the left. 

[Presentation and Workshop](https://www.fairkom.eu/presenting-label-commons-public-license-at-ars-electronica-festival) at Ars Electronica Festival Linz 2019-09-09 10 a.m.